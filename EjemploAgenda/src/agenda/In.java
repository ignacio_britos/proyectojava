package agenda;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import java.util.StringTokenizer;
import java.util.NoSuchElementException;

public class In {

    private static StringTokenizer st;

    private static BufferedReader source;

    private In() {
    }

    public static String readLine() {
        return getNextToken("\r\n\f");
    }

    public static int readInt() {
        return (int) readLong();
    }

    public static long readLong() {
        long retVal = 0;
        try {
            retVal = Long.parseLong(getNextToken());
        } catch (NumberFormatException e) {
        }
        return retVal;
    }

    public static float readFloat() {
        return (float) readDouble();
    }

    public static double readDouble() {
        double retVal = 0.0;
        try {
            retVal = Double.valueOf(getNextToken()).doubleValue();
        } catch (NumberFormatException e) {
        }
        return retVal;
    }

    public static char readChar() {
        char car = '\0';
        String str;

        str = getNextToken("\0");
        if (str.length() > 0) {
            car = str.charAt(0);
            st = new StringTokenizer(str.substring(1));
        }
        return car;
    }

    private static String getNextToken() {
        return getNextToken(null);
    }

    private static String getNextToken(String delim) {
        String input;
        String retVal = "";

        try {
            if ((st == null) || !st.hasMoreElements()) {
                if (source == null) {
                    source = new BufferedReader(new InputStreamReader(System.in));
                }
                input = source.readLine();
                st = new StringTokenizer(input);
            }
            if (delim == null) {
                delim = " \t\n\r\f";
            }
            retVal = st.nextToken(delim);
        } catch (NoSuchElementException e1) {

        } catch (IOException e2) {

        }

        return retVal;
    }
}
