
package agenda.archivo;

import agenda.listas.*;
import java.io.Serializable;

public class Datos implements Serializable
{
    private ListaContactos mContactos;
    private ListaCategorias mCategorias;

    protected Datos()
    {
        mContactos = new ListaContactos();
        mCategorias = new ListaCategorias();
    }

    public ListaCategorias getCategorias()
    {
        return mCategorias;
    }

    public ListaContactos getContactos()
    {
        return mContactos;
    }
    
    

}
