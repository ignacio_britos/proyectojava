

package agenda.listas;

import agenda.objetos.Categoria;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;


public class ListaCategorias implements Serializable
{
    private LinkedList<Categoria> mCategorias;
    private Hashtable<Integer, Categoria> mIdxCategorias;

    public ListaCategorias()
    {
        mCategorias = new LinkedList<Categoria>();
        mIdxCategorias = new Hashtable<Integer, Categoria>(50);
    }

    public int getCantidad()
    {
        return mCategorias.size();
    }

    public void Agregar(Categoria pValor)
    {
        mCategorias.add(pValor);
        mIdxCategorias.put(pValor.getNumero(), pValor);

    }

    @Override
    public String toString()
    {
        StringBuffer buff = new StringBuffer();
        buff.append("\n\nLista de Categorias\n");
        for(Categoria item : mCategorias)
            buff.append(item + "\n");

        return buff.toString();
    }

    public Categoria buscarNro(int pNro)
    {
        return mIdxCategorias.get(pNro);

    }
    
    public List<Categoria> todas()
    {
        return mCategorias;
    }

}
