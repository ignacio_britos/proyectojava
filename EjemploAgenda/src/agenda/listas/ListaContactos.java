
package agenda.listas;

import agenda.objetos.Contacto;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;


public class ListaContactos implements Serializable
{

    private LinkedList<Contacto> mContactos;
    private Hashtable<String, Contacto> mIdxContactos;

    public ListaContactos()
    {
        mContactos = new LinkedList<Contacto>();
        mIdxContactos = new Hashtable<String, Contacto>();
    }

    public void agregar(Contacto pValor)
    {
        mContactos.add(pValor);
        mIdxContactos.put(pValor.getNick(), pValor);

    }

    public int getCantidad()
    {
        return mContactos.size();
    }

    public Contacto getItem(int index)
    {
        return mContactos.get(index);
    }

    @Override
    public String toString()
    {
        StringBuffer buff = new StringBuffer();
        buff.append("\n\nLista de Contactos\n");
        if (mContactos.size() > 0)
        {
            TreeSet<Contacto> tree = new TreeSet<Contacto>(mContactos);
            for (Contacto item : tree)
            {
                buff.append("\n" + item);
            }
        }
        else
        {
            buff.append("\nNo hay contactos cargados...");
        }

        return buff.toString();
    }

    public Contacto buscar(String pNick)
    {
        return mIdxContactos.get(pNick);

    }

    public LinkedList<Contacto> filtrar(String pFiltro)
    {
        TreeSet<Contacto> aux = new TreeSet<Contacto>();

        for (Contacto item : mContactos)
        {
            if (item.pertenece(pFiltro))
            {
                aux.add(item);
            }
        }

        return new LinkedList<Contacto>(aux);
    }

    public List<Contacto> todos()
    {
        ArrayList<Contacto> al = new ArrayList<Contacto>(new TreeSet(mContactos));
        return al;

    }
}
