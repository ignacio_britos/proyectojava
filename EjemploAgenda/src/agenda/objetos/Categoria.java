
package agenda.objetos;

import agenda.listas.ListaContactos;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;


public class Categoria implements Serializable
{

    private int mNumero;
    private String mNombre;
    private ListaContactos mContactos;

    public String getNombre()
    {
        return mNombre;
    }

    public Categoria(int mNumero, String mNombre)
    {
        this.mNumero = mNumero;
        this.mNombre = mNombre;
        this.mContactos = new ListaContactos();

    }

    public void setNombre(String mNombre)
    {
        this.mNombre = mNombre;
    }

    public int getNumero()
    {
        return mNumero;
    }

   
    public Contacto getContacto(int pIndex)
    {
        Contacto aux = null;
        if (pIndex >= 0 && pIndex < mContactos.getCantidad()) //Chequeo que el índice dado esté entre los valores de índices con contactos agregados
        {
            aux = mContactos.getItem(pIndex);
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
        return aux;
    }

    
    public List<Contacto> getContactos()
    {
        return mContactos.todos();
    }

   
    public void agregar(Contacto pCto)
    {
        mContactos.agregar(pCto);
    }
    
    public List<Contacto> filtrar(String pFiltro)
    {
        return mContactos.filtrar(pFiltro);
    }

    public String toString()
    {
        String res = mNumero + " - " + mNombre + " (" + mContactos.getCantidad() + ")";
        return res;
    }
}
