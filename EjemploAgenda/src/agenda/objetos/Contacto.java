
package agenda.objetos;

import java.io.InvalidClassException;
import java.io.Serializable;


public class Contacto implements Serializable, Comparable
{
    private String mNick;
    private String mNombre;
    private String mCelular;
    private String mMail;
    private String mTexto;

    public String getCelular()
    {
        return mCelular;
    }

    public String getMail()
    {
        return mMail;
    }

    public String getNick()
    {
        return mNick;
    }

    public String getNombre()
    {
        return mNombre;
    }

    public String getTexto()
    {
        return mTexto;
    }

    public void setCelular(String mCelular)
    {
        this.mCelular = mCelular;
    }

    public void setMail(String mMail)
    {
        this.mMail = mMail;
    }

    public void setNick(String mNick)
    {
        this.mNick = mNick;
    }

    public void setNombre(String mNombre)
    {
        this.mNombre = mNombre;
    }

    public void setTexto(String mTexto)
    {
        this.mTexto = mTexto;
    }

    public Contacto(String pNick, String pMail)
    {
        this(pNick, pMail, "");
    }

    public Contacto(String pNick, String pMail, String pCelular)
    {
        this.mNick = pNick;
        this.mCelular = pCelular;
        this.mMail = pMail;
    }

    @Override
    public String toString()
    {
        StringBuffer buff = new StringBuffer();
        buff.append("**** " + mNick + "\n");
        buff.append(mNombre + "\n");
        buff.append("Celu: " + mCelular + "\n");
        buff.append("Mail: " + mMail +"\n");
        buff.append("Notas\n" + mTexto + "\n");

        return buff.toString();
    }
 
    @Override
    public boolean equals(Object obj)
    {
        boolean res = false;
        if (obj instanceof String && mNick.equalsIgnoreCase((String)obj))
        {
            res = true;
        }
        else if (obj instanceof Contacto && mNick.equalsIgnoreCase(((Contacto)obj).getNick()))
        {
            res = true;
        }

        return res;
    }

    
    public boolean pertenece(String pFiltro)
    {
        boolean res = false;
        if (mNick.toLowerCase().contains(pFiltro.toLowerCase()) || 
            (mNombre != null && mNombre.toLowerCase().contains(pFiltro.toLowerCase())) ||
            (mMail != null && mMail.toLowerCase().contains(pFiltro.toLowerCase())) ||
            (mTexto != null && mTexto.toLowerCase().contains(pFiltro.toLowerCase())))
            res = true;

        return res;
    }

    public int compareTo(Object o)
    {
        int res;
        if (o instanceof Contacto)
        {
            res = this.mNombre.compareTo(((Contacto) o).getNombre());
        }
        else
            throw new RuntimeException("El tipo del objeto debe ser Contacto.");
        
        return res;
    }
}


