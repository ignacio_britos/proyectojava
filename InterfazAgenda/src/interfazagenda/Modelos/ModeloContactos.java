
package interfazagenda.Modelos;

import agenda.objetos.Contacto;
import java.util.LinkedList;
import java.util.List;
import javax.swing.table.AbstractTableModel;


public class ModeloContactos extends AbstractTableModel
{

    private String[] mColumnas = {"Nick", "Nombre", "Tel.", "Mail"};
    private List<Contacto> mContactos;

    public ModeloContactos()
    {
        mContactos = new LinkedList<Contacto>();
    }

    public ModeloContactos(List<Contacto> lista)
    {
        mContactos = lista;
    }

    @Override
    public int getRowCount()
    {
        int wRes = 25;
        if (mContactos.size() > 25)
        {
            wRes = mContactos.size();
        }
        return wRes;
    }

    @Override
    public int getColumnCount()
    {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        String res = "";
        if (rowIndex < mContactos.size())
        {
            Contacto aux = mContactos.get(rowIndex);
            switch (columnIndex)
            {
                case 0:
                    res = aux.getNick();
                    break;
                case 1:
                    res = aux.getNombre();
                    break;
                case 2:
                    res = aux.getCelular();
                    break;
                case 3:
                    res = aux.getMail();
                    break;

            }
        }
        return res;
    }

    @Override
    public String getColumnName(int column)
    {
        return mColumnas[column];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return false;
    }

    public Contacto getItem(int pRowIndex)
    {
        Contacto wResp = null;
        if(pRowIndex < mContactos.size())
            wResp = mContactos.get(pRowIndex);
        return wResp;
    }
}
