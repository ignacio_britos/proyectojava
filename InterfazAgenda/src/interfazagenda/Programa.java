
package interfazagenda;

import agenda.archivo.Archivo;
import interfazagenda.gui.FrmPrincipal;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;


public class Programa {

   
    public static void main(String[] args) {

        Archivo.cargar();

        try {
            
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException e) {
        } catch (ClassNotFoundException e) {
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        }

        FrmPrincipal form = new FrmPrincipal();
        form.setVisible(true);


    }
}
