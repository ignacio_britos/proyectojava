package interfazagenda.gui;

import agenda.archivo.Archivo;
import agenda.listas.ListaCategorias;
import agenda.listas.ListaContactos;
import agenda.objetos.Categoria;
import agenda.objetos.Contacto;
import interfazagenda.Modelos.ModeloContactos;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class FrmPrincipal extends javax.swing.JFrame {

    private Categoria mCatSelected = null;

    public FrmPrincipal() {
        initComponents();

        cargarCategorias();

        ListaContactos lista = Archivo.getDatos().getContactos();
        setearGrilla(lista.todos());

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jToolBar1 = new javax.swing.JToolBar();
        btnAgregar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnBorrar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        lblBuscar = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        pnlCategoria = new javax.swing.JPanel();
        btnNuevaCat = new javax.swing.JButton();
        txtNuevaCat = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        treeCategorias = new javax.swing.JTree();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblContactos = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Agenda de Contactos");
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });

        jToolBar1.setRollover(true);

        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfazagenda/images/Contacto.png"))); // NOI18N
        btnAgregar.setToolTipText("Agregar contacto");
        btnAgregar.setFocusable(false);
        btnAgregar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAgregar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAgregar.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnAgregarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnAgregar);

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfazagenda/images/Editar.png"))); // NOI18N
        btnEditar.setToolTipText("Editar Contacto");
        btnEditar.setFocusable(false);
        btnEditar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEditar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEditar.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnEditarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnEditar);

        btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfazagenda/images/Borrar.png"))); // NOI18N
        btnBorrar.setToolTipText("Borrar Contacto");
        btnBorrar.setFocusable(false);
        btnBorrar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnBorrar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(btnBorrar);
        jToolBar1.add(jSeparator1);

        lblBuscar.setText("Buscar:  ");
        jToolBar1.add(lblBuscar);

        txtBuscar.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jToolBar1.add(txtBuscar);

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfazagenda/images/Find.png"))); // NOI18N
        btnBuscar.setToolTipText("Buscar");
        btnBuscar.setFocusable(false);
        btnBuscar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnBuscar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnBuscar.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnBuscarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnBuscar);

        getContentPane().add(jToolBar1, java.awt.BorderLayout.PAGE_START);

        jSplitPane1.setDividerLocation(160);

        btnNuevaCat.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfazagenda/images/Categoria.png"))); // NOI18N
        btnNuevaCat.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnNuevaCatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlCategoriaLayout = new javax.swing.GroupLayout(pnlCategoria);
        pnlCategoria.setLayout(pnlCategoriaLayout);
        pnlCategoriaLayout.setHorizontalGroup(
            pnlCategoriaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlCategoriaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtNuevaCat, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(btnNuevaCat, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnlCategoriaLayout.setVerticalGroup(
            pnlCategoriaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCategoriaLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(pnlCategoriaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnNuevaCat, javax.swing.GroupLayout.Alignment.TRAILING, 0, 0, Short.MAX_VALUE)
                    .addComponent(txtNuevaCat, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        treeCategorias.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener()
        {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt)
            {
                treeCategoriasValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(treeCategorias);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
            .addComponent(pnlCategoria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(pnlCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(jPanel1);

        jScrollPane2.setBackground(new java.awt.Color(255, 255, 255));

        tblContactos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String []
            {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblContactos.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                tblContactosMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblContactos);

        jSplitPane1.setRightComponent(jScrollPane2);

        getContentPane().add(jSplitPane1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevaCatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevaCatActionPerformed
        if (txtNuevaCat.getText() != null && !txtNuevaCat.getText().isEmpty()) {
            ListaCategorias lista = Archivo.getDatos().getCategorias();
            int newNum = lista.getCantidad() + 1;
            Categoria aux = new Categoria(newNum, txtNuevaCat.getText());
            lista.Agregar(aux);
            txtNuevaCat.setText("");
            cargarCategorias();
        } else {
            JOptionPane.showMessageDialog(this, "Debe ingresar el nombre de la categoria", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnNuevaCatActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Archivo.guardar();
    }//GEN-LAST:event_formWindowClosing

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        FormContacto form = new FormContacto();
        form.setPadre(this);
        form.setVisible(true);
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void treeCategoriasValueChanged(javax.swing.event.TreeSelectionEvent evt)//GEN-FIRST:event_treeCategoriasValueChanged
    {//GEN-HEADEREND:event_treeCategoriasValueChanged
        txtBuscar.setText("");
        if (((DefaultMutableTreeNode) treeCategorias.getLastSelectedPathComponent()).getUserObject() instanceof Categoria) {
            mCatSelected = (Categoria) ((DefaultMutableTreeNode) treeCategorias.getLastSelectedPathComponent()).getUserObject();
            setearGrilla(mCatSelected.getContactos());
        } else {
            mCatSelected = null;
            ListaContactos lista = Archivo.getDatos().getContactos();
            setearGrilla(lista.todos());
        }
    }//GEN-LAST:event_treeCategoriasValueChanged

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnBuscarActionPerformed
    {//GEN-HEADEREND:event_btnBuscarActionPerformed
        refrescar();

    }//GEN-LAST:event_btnBuscarActionPerformed

    private void tblContactosMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblContactosMouseClicked
    {//GEN-HEADEREND:event_tblContactosMouseClicked
        if (evt.getButton() == evt.BUTTON1) {
            if (evt.getClickCount() == 2) {
                Contacto cto = ((ModeloContactos) tblContactos.getModel()).getItem(tblContactos.getSelectedRow());
                if (cto != null) {
                    JOptionPane.showMessageDialog(this, cto, "Información de Contacto", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
    }//GEN-LAST:event_tblContactosMouseClicked

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnEditarActionPerformed
    {//GEN-HEADEREND:event_btnEditarActionPerformed
        Contacto cto = ((ModeloContactos) tblContactos.getModel()).getItem(tblContactos.getSelectedRow());
        if (cto != null) {
            FormEditContacto frm = new FormEditContacto(cto);
            frm.setPadre(this);
            frm.setVisible(true);
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmPrincipal().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnNuevaCat;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel lblBuscar;
    private javax.swing.JPanel pnlCategoria;
    private javax.swing.JTable tblContactos;
    private javax.swing.JTree treeCategorias;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JTextField txtNuevaCat;
    // End of variables declaration//GEN-END:variables

    public void cargarCategorias() {
        DefaultMutableTreeNode raiz = new DefaultMutableTreeNode();
        raiz.setUserObject("Todas");

        ListaCategorias lista = Archivo.getDatos().getCategorias();
        for (Categoria item : lista.todas()) {
            DefaultMutableTreeNode aux = new DefaultMutableTreeNode(item);
            raiz.add(aux);
        }

        DefaultTreeModel modelo = new DefaultTreeModel(raiz);

        treeCategorias.setModel(modelo);

    }

    private void setearGrilla(List<Contacto> lista) {
        ModeloContactos modeloCto = new ModeloContactos(lista);
        tblContactos.setModel(modeloCto);

    }

    public void refrescar() {
        if (!txtBuscar.getText().isEmpty()) {
            List<Contacto> res;
            if (mCatSelected == null) {
                res = Archivo.getDatos().getContactos().filtrar(txtBuscar.getText());
            } else {
                res = mCatSelected.filtrar(txtBuscar.getText());
            }
            setearGrilla(res);

        } else {
            if (mCatSelected == null) {
                setearGrilla(Archivo.getDatos().getContactos().todos());
            } else {
                setearGrilla(mCatSelected.getContactos());
            }

        }

    }
}
